(function (document, Drupal) {

  'use strict';

  Drupal.tabbing = {};

  Drupal.tabbing.init = false;

  Drupal.tabbing.handleFirstTab = function(e) {
    // the "I am a keyboard user" Tab key
    if (e.key === 'Tab') {
      document.body.classList.remove('user-has-not-been-tabbing');
      document.body.classList.add('user-is-tabbing');
      window.removeEventListener('keyup', Drupal.tabbing.handleFirstTab);
    }
  };

  Drupal.behaviors.compony_handleFirstTab = {
    attach: function (context) {
      if (!Drupal.tabbing.init) {
        document.body.classList.add('user-has-not-been-tabbing');
        window.addEventListener('keyup', Drupal.tabbing.handleFirstTab);
      }
      Drupal.tabbing.init = true;
    }
  };

})(document, Drupal);
